package music.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import music.dao.AlbumDAO;
import music.domain.Album;
import music.domain.Song;



@RestController
public class AlbumREST {
	private Logger logger = Logger.getLogger(AlbumREST.class.getSimpleName());

	@Autowired
	AlbumDAO albumDAO;
	
	@RequestMapping(value = "/albumAdd", method = RequestMethod.GET)
	public void addAlbum(@RequestBody Album album)
			throws IOException {
		albumDAO.save(album);
	}
	
	@RequestMapping(value = "/albumList", method = RequestMethod.GET)
	public @ResponseBody List<Album> listAlbum()
			throws IOException {
		Album album1 = new Album();
		Song song = new Song();
		song.setName("For Whom The Bells Tolls");
		album1.setName("Black Album");
		List<Song> musicasAlbum = new ArrayList();
		musicasAlbum.add(song);
		album1.setSongs(musicasAlbum);
		albumDAO.save(album1);
		logger.info(albumDAO.findAll().toString());
		List<Album> lista = (List<Album>) albumDAO.findAll();
		return lista;
	}
	
	@RequestMapping(value = "/album", method = RequestMethod.GET)
	public @ResponseBody Album album()
			throws IOException {
		Album album1 = new Album();
		Song song = new Song();
		song.setName("For Whom The Bells Tolls");
		
		album1.setName("Black Album");
		List<Song> musicasAlbum = new ArrayList();
		musicasAlbum.add(song);
		album1.setSongs(musicasAlbum);
		albumDAO.save(album1);
		logger.info("entrou aqui");
		return album1;
	}
	
}
